# Music Player Playlists

Proyecto que contiene la funcionalidad para administrar listas de reproducción de los usuarios.

## Diseño conceptual

Se propone el siguiente diseño conceptual para el manejo de las listas de reproducción.

![IMAGE_DESCRIPTION](https://sigma-resources-dev.s3.amazonaws.com/quipux_2.png)

Se manejarán dos rutas para administar las listas de reproducción.


| Ruta           | Descripción                                                   |
|----------------|---------------------------------------------------------------|
|/playlist       | Vista para visualizar las listas que ha creado el usuario. Se podr án crear nuevas listas desde esta vista    |
|/playlist/:name | Vista para visualizar el detalle de las listas de reproduccion. Se podrán añadir y eliminar canciones a la lista. Se podrá eliminar la lista|

## Diagrama arquitectónico y de despliegue

![IMAGE_DESCRIPTION](https://sigma-resources-dev.s3.amazonaws.com/quipux_1.png)

### Despliegue

Se desplegará la solución haciendo uso de la nube de AWS. Se pretende conectar el repositorio con el servicio CodeBuild donde se ejecutarán los pipelines de calidad de código y construcción del proyecto. Posteriormente haciendo uso de code deploy se publicará los archivos estáticos del proyecto en S3.

### Arquitectura

Se usará el servicio Route53 de aws para administrar los dominios desde donde se podrá acceder a los microfrontend desplegados en AWS a través de cloudfront. Cloudfront se encargará de entregar los archivos estáticos desde el bucket de S3 donde estarán desplegados. De esta forma facilmente se podrá actualizar y desplegar los componentes de la aplicación.

Dentro de los proyectos frontend se aplicará arquitectura limpia, el modelo de negocio se extraerá en una librería de typescript, de modo que no se dependa de ningun framework del lado del front y se creará una librería de angular (react, vue, etc.) que ejecute el modelo de negocio dependiendo de las necesidades. De esta forma cambiar de framework o desarrollar microfrontends en varios frameworks no requerirá alterar o construir nuevamente casos de uso que ya estén en la librería del modelo de negocio.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


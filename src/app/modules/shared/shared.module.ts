import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpDataSource } from 'src/app/data/data-source/http.data-source';
import { HttpImplService } from 'src/app/services/http-impl.service';
import { HttpClient } from '@angular/common/http';
import { UserRepository } from 'src/app/domain/repository/user.repository';
import { UserRepositoryImpl } from 'src/app/data/repository/user-impl.repository';
import { CreateUserUseCase } from 'src/app/domain/use-case/user/create-user.use-case';
import { SignInUseCase } from 'src/app/domain/use-case/auth/sign-in.use-case';
import { AuthRepository } from 'src/app/domain/repository/auth.repository';
import { AuthRepositoryImpl } from 'src/app/data/repository/auth-impl.repository';
import { CreatePlaylistUseCase } from 'src/app/domain/use-case/playlist/create-playlist.use-case';
import { PlaylistRepository } from 'src/app/domain/repository/playlist.repository';
import { PlaylistRepositoryImpl } from 'src/app/data/repository/playlist-impl.repository';
import { GetPlaylistsUseCase } from 'src/app/domain/use-case/playlist/get-playlists.use-case';
import { GetPlaylistUseCase } from 'src/app/domain/use-case/playlist/get-playlist.use-case';
import { DeleteSongUseCase } from 'src/app/domain/use-case/song/delete-song.use-case';
import { SongRepository } from 'src/app/domain/repository/song.repository';
import { SongRepositoryImpl } from 'src/app/data/repository/song-impl.repository';
import { CreateSongUseCase } from 'src/app/domain/use-case/song/create-song.use-case';
import { DeletePlaylistUseCase } from 'src/app/domain/use-case/playlist/delete-playlist.use-case';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: HttpDataSource,
      useClass: HttpImplService,
      deps: [HttpClient]
    },
    {
      provide: UserRepository,
      useClass: UserRepositoryImpl,
      deps: [ HttpDataSource ]
    },
    {
      provide: AuthRepository,
      useClass: AuthRepositoryImpl,
      deps: [ HttpDataSource ]
    },
    {
      provide: PlaylistRepository,
      useClass: PlaylistRepositoryImpl,
      deps: [ HttpDataSource ]
    },
    {
      provide: SongRepository,
      useClass: SongRepositoryImpl,
      deps: [ HttpDataSource ]
    },
    {
      provide: CreateUserUseCase,
      deps: [ UserRepository ]
    },
    {
      provide: SignInUseCase,
      deps: [ AuthRepository ]
    },
    {
      provide: CreatePlaylistUseCase,
      deps: [ PlaylistRepository ]
    },
    {
      provide: GetPlaylistsUseCase,
      deps: [ PlaylistRepository ]
    },
    {
      provide: GetPlaylistUseCase,
      deps: [ PlaylistRepository ]
    },
    {
      provide: DeletePlaylistUseCase,
      deps: [ PlaylistRepository ]
    },
    {
      provide: DeleteSongUseCase,
      deps: [ SongRepository ]
    },
    {
      provide: CreateSongUseCase,
      deps: [ SongRepository ]
    }
  ]
})
export class SharedModule { }

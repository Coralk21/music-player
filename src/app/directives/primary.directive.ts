import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appPrimary]',
  standalone: true
})
export class PrimaryDirective {

  @HostListener('mouseenter') onMouseEnter() {
    this.changeColor('#177ddc')
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.changeColor('rgba(255, 255, 255, 0.85)')
  }

  constructor(private element: ElementRef<HTMLElement>) {}

  private changeColor(color: string) {
    this.element.nativeElement.style.color = color;
  }
}

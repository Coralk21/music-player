import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Store } from "@ngrx/store";
import { lastValueFrom } from "rxjs";
import { AppState } from "../store/app.reducer";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {

  token?: string;

  constructor(
    private router: Router,
    private store: Store<AppState>
  ) {
    this.store.select('token').subscribe({
      next: (( { value } ) => this.token = value)
    });
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    _: RouterStateSnapshot): boolean
  {
    if(!route.data['public'] && !this.token || route.data['public'] && this.token) {
      if (route.data['redirectTo']) {
        this.router.navigate(route.data['redirectTo']);
      }
      return false;
    }
    return true;
  }
}
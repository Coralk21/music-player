import { NewUserInput, NewUserOutput } from "../../model/user";
import { UserRepository } from "../../repository/user.repository";

export class CreateUserUseCase {
    constructor(private userRepository: UserRepository) {}

    execute(user: NewUserInput): Promise<NewUserOutput> {
        return this.userRepository.createUser(user);
    }
}
import { Playlist, PlaylistInput } from "../../model/playlist";
import { PlaylistRepository } from "../../repository/playlist.repository";

export class GetPlaylistsUseCase {
    constructor(private playlistRepository: PlaylistRepository) {}

    execute(): Promise<Playlist[]> {
        return this.playlistRepository.getPlaylists();
    }
}
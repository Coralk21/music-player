import { Playlist } from "../../model/playlist";
import { PlaylistRepository } from "../../repository/playlist.repository";

export class DeletePlaylistUseCase {
    constructor(private playlistRepository: PlaylistRepository) {}

    execute(name: string): Promise<void> {
        return this.playlistRepository.deletePlaylist(name);
    }
}
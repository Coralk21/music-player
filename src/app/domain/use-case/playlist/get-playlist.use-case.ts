import { Playlist } from "../../model/playlist";
import { PlaylistRepository } from "../../repository/playlist.repository";

export class GetPlaylistUseCase {
    constructor(private playlistRepository: PlaylistRepository) {}

    execute(name: string): Promise<Playlist> {
        return this.playlistRepository.getPlaylist(name);
    }
}
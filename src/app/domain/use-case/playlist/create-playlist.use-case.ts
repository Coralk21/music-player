import { Playlist, PlaylistInput } from "../../model/playlist";
import { PlaylistRepository } from "../../repository/playlist.repository";

export class CreatePlaylistUseCase {
    constructor(private playlistRepository: PlaylistRepository) {}

    execute(playlist: PlaylistInput): Promise<Playlist> {
        return this.playlistRepository.createPlaylist(playlist);
    }
}
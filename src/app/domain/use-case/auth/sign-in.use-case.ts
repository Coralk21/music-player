import { Credentials } from "../../model/credentials";
import { Token } from "../../model/token";
import { AuthRepository } from "../../repository/auth.repository";

export class SignInUseCase {
    constructor(private repository: AuthRepository) {}

    execute(credentials: Credentials): Promise<Token> {
        return this.repository.signIn(credentials);
    }
}
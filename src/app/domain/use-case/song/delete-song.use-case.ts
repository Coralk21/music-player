import { SongRepository } from "../../repository/song.repository";


export class DeleteSongUseCase {
    constructor(private playlistRepository: SongRepository) {}

    execute(id: number): Promise<{ message: string }> {
        return this.playlistRepository.deleteSong(id);
    }
}
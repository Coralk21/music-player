import { Song } from "../../model/song";
import { SongRepository } from "../../repository/song.repository";


export class CreateSongUseCase {
    constructor(private playlistRepository: SongRepository) {}

    execute(idPlaylist: number, song: Song): Promise<Song> {
        return this.playlistRepository.createSong(idPlaylist, song);
    }
}
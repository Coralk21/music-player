export interface SongInput {
    titulo: string;
    artista: string;
    album: string;
    anno: string;
    genero: string;
}

export interface Song extends SongInput {
    id: number;
}
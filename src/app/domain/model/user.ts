import { Token } from "./token";

export interface NewUserInput {
    email: string;
    password: string;
    photo: string;
    first_name: string;
    last_name: string;
}

export interface NewUserOutput {
    id: number;
    email: string;
    auth: Token;
    first_Name: string;
    last_Name: string;
}
import { Song, SongInput } from "./song";

export interface PlaylistInput {
    nombre: string;
    descripcion: string;
    canciones: SongInput[];
}

export interface Playlist {
    id: number;
    nombreLista: string;
    descripcion: string;
    songs: Song[];
    location: string;
}
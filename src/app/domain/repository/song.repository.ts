import { Playlist, PlaylistInput } from "../model/playlist";
import { Song } from "../model/song";

export abstract class SongRepository {
    abstract deleteSong(id: number): Promise<{ message: string }>;
    abstract createSong(idPlaylist: number, song: Song): Promise<Song>;
}
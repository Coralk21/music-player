import { Credentials } from "../model/credentials";
import { Token } from "../model/token";

export abstract class AuthRepository {
    abstract signIn(credentials: Credentials): Promise<Token>;
}
import { NewUserInput, NewUserOutput } from "../model/user";

export abstract class UserRepository {
    abstract createUser(user: NewUserInput): Promise<NewUserOutput>;
}
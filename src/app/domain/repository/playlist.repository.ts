import { Playlist, PlaylistInput } from "../model/playlist";

export abstract class PlaylistRepository {
    abstract createPlaylist(playlist: PlaylistInput): Promise<Playlist>;
    abstract getPlaylists(): Promise<Playlist[]>;
    abstract getPlaylist(name: string): Promise<Playlist>;
    abstract deletePlaylist(name: string): Promise<void>;
}
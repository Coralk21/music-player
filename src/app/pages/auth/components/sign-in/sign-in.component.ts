import { Component } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { SignInUseCase } from 'src/app/domain/use-case/auth/sign-in.use-case';
import { saveToken } from 'src/app/store/token/token.actions';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less']
})
export class SignInComponent {
  signInForm: FormGroup;

  constructor(
    private signIn: SignInUseCase,
    private router: Router,
    private store: Store,
    private fb: NonNullableFormBuilder
  ) {
    this.signInForm = this.fb.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  async submitForm(): Promise<void> {
    if (this.signInForm.valid) {
      const result = await this.signIn.execute(this.signInForm.value);
      this.store.dispatch(saveToken(result));
      this.signInForm.reset();
      this.router.navigate(['/', 'player', 'playlist']);
    } else {
      Object.values(this.signInForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
}

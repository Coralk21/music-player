import { Component } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { CreateUserUseCase } from 'src/app/domain/use-case/user/create-user.use-case';
import { saveToken } from 'src/app/store/token/token.actions';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.less']
})
export class SignUpComponent {
  signUpForm: FormGroup;

  constructor(
    private fb: NonNullableFormBuilder,
    private router: Router,
    private store: Store,
    private createUserUseCase: CreateUserUseCase
  ) {
    this.signUpForm = this.fb.group({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      photo: new FormControl('', [
        Validators.required,
        Validators.pattern(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/)
      ])
    });
  }

  async submitForm(): Promise<void> {
    if (this.signUpForm.valid) {
      const result = await this.createUserUseCase.execute(this.signUpForm.value);
      this.store.dispatch(saveToken(result.auth));
      this.signUpForm.reset();
      this.router.navigate(['/', 'player', 'playlist']);
    } else {
      Object.values(this.signUpForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { HttpDataSource } from 'src/app/data/data-source/http.data-source';
import { HttpClient } from '@angular/common/http';
import { HttpImplService } from 'src/app/services/http-impl.service';
import { UserRepository } from 'src/app/domain/repository/user.repository';
import { UserRepositoryImpl } from 'src/app/data/repository/user-impl.repository';
import { StoreModule } from '@ngrx/store';


@NgModule({
  declarations: [
    AuthComponent,
    SignInComponent,
    SignUpComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    SharedModule,
    StoreModule
  ]
})
export class AuthModule { }

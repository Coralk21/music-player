import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Playlist, PlaylistInput } from 'src/app/domain/model/playlist';
import { CreatePlaylistUseCase } from 'src/app/domain/use-case/playlist/create-playlist.use-case';
import { GetPlaylistsUseCase } from 'src/app/domain/use-case/playlist/get-playlists.use-case';
import { MessageService } from 'src/app/services/message.service';


type Song = FormGroup<{
  titulo: FormControl<string>;
  album: FormControl<string>;
  anno: FormControl<string>;
  artista: FormControl<string>;
  genero: FormControl<string>;
}>

@Component({
  selector: 'app-page-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.less']
})
export class PlaylistPageComponent implements OnInit {
  isVisibleNewPlaylist = false;
  isCreating = false;
  newPlaylistError = '';
  playlists: Playlist[] = [];

  newPlaylistForm;

  get songs() {
    return this.newPlaylistForm.controls.canciones as FormArray<Song>;
  }

  constructor(
    private fb: NonNullableFormBuilder,
    private createPlaylistUseCase: CreatePlaylistUseCase,
    private getPlaylists: GetPlaylistsUseCase,
    private router: Router
  ) {
    this.newPlaylistForm = this.generateForm();
  }

  ngOnInit(): void {
    this.loadPlaylists();
  }

  async loadPlaylists() {
    try {
      this.playlists = await this.getPlaylists.execute();
    } catch (e) {
      console.error(e);
    }
  }

  generateForm() {
    return new FormGroup({
      nombre: this.fb.control('', Validators.required),
      descripcion: this.fb.control('', Validators.required),
      canciones: this.fb.array<Song>([])
    })
  }

  toggleNewPlaylist() {
    this.newPlaylistForm = this.generateForm();
    this.newPlaylistError = '';
    this.isVisibleNewPlaylist = !this.isVisibleNewPlaylist;
  }

  async createPlaylist() {
    if (this.newPlaylistForm.valid) {
      this.isCreating = true;
      try {
        const result = await this.createPlaylistUseCase.execute(this.newPlaylistForm.value as PlaylistInput);
        this.playlists.push(result);
        this.toggleNewPlaylist();
      } catch (e: any) {
        if (e.status === 409) {
          this.newPlaylistError = 'Ya existe una playlist con el nombre ' + this.newPlaylistForm.controls.nombre.value;
        } else {
          this.newPlaylistError = 'Ha ocurrido un error, intenta mas tarde.';
        }
      } finally {
        this.isCreating = false;
      }
    } else {
      Object.values(this.newPlaylistForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.markAsTouched();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      this.songs.controls.forEach((control: Song) => {
        Object.values(control.controls).forEach((control) => {
          if (control.invalid) {
            control.markAsDirty();
            control.markAsTouched();
            control.updateValueAndValidity({ onlySelf: false });
          }
        })
      });
    }
  }

  insertSong() {
    this.songs.push(this.getNewSongControl());
  }

  getNewSongControl() {
    return this.fb.group({
      titulo: this.fb.control('', Validators.required),
      album: this.fb.control('', Validators.required),
      anno: this.fb.control('', Validators.required),
      artista: this.fb.control('', Validators.required),
      genero: this.fb.control('', Validators.required),
    });
  }

  goToDetail(playlist: Playlist) {
    this.router.navigate(['/', 'player', 'playlist', playlist.nombreLista ]);
  }
}

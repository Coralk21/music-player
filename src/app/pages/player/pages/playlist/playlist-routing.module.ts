import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaylistPageComponent } from './playlist.component';

const routes: Routes = [
  { path: '', component: PlaylistPageComponent },
  { path: ':name', loadChildren: () => import('../playlist-detail/playlist-detail.module').then(m => m.PlaylistDetailModule) }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistRoutingModule { }

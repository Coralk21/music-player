import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistRoutingModule } from './playlist-routing.module';
import { PlaylistPageComponent } from './playlist.component';
import { PlaylistComponent } from 'src/app/standalone/playlist/playlist.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';


@NgModule({
  declarations: [
    PlaylistPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PlaylistRoutingModule,
    PlaylistComponent,
    NzButtonModule,
    NzIconModule,
    NzPopoverModule,
    NzModalModule,
    NzFormModule,
    NzInputModule
  ]
})
export class PlaylistModule { }

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Song } from 'src/app/domain/model/song';

@Component({
  selector: 'app-playlist-songs',
  templateUrl: './playlist-songs.component.html',
  styleUrls: ['./playlist-songs.component.less']
})
export class PlaylistSongsComponent {
  @Input() songs: Song[] = [];
  @Output() delete: EventEmitter<Song> = new EventEmitter<Song>();

  deleteSong(song: Song) {
    this.delete.emit(song);
  }
}

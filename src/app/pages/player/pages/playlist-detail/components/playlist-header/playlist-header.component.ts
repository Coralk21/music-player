import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Playlist } from 'src/app/domain/model/playlist';

@Component({
  selector: 'app-playlist-header',
  templateUrl: './playlist-header.component.html',
  styleUrls: ['./playlist-header.component.less']
})
export class PlaylistHeaderComponent {
  @Input() playlist!: Playlist;
  @Output() delete: EventEmitter<void> = new EventEmitter();

  deleteList() {
    this.delete.emit();
  }
}

import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Song } from 'src/app/domain/model/song';

@Component({
  selector: 'app-new-song',
  templateUrl: './new-song.component.html',
  styleUrls: ['./new-song.component.less']
})
export class NewSongComponent {
  newSongForm = new FormGroup({
    titulo: new FormControl('', Validators.required),
    album: new FormControl('', Validators.required),
    anno: new FormControl('', Validators.required),
    artista: new FormControl('', Validators.required),
    genero: new FormControl('', Validators.required),
  })
  @Output() create: EventEmitter<Song> = new EventEmitter<Song>();

  createNewSong() {
    if (this.newSongForm.valid) {
      this.create.emit(this.newSongForm.value as Song);
    } else {
      Object.values(this.newSongForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.markAsTouched();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
}

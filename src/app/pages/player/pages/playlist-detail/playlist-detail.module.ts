import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistDetailRoutingModule } from './playlist-detail-routing.module';
import { PlaylistDetailComponent } from './playlist-detail.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { PlaylistHeaderComponent } from './components/playlist-header/playlist-header.component';
import { PlaylistSongsComponent } from './components/playlist-songs/playlist-songs.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NewSongComponent } from './components/new-song/new-song.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { ReactiveFormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzModalModule } from 'ng-zorro-antd/modal';


@NgModule({
  declarations: [
    PlaylistDetailComponent,
    PlaylistHeaderComponent,
    PlaylistSongsComponent,
    NewSongComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PlaylistDetailRoutingModule,
    NzIconModule,
    NzTableModule,
    NzFormModule,
    NzButtonModule,
    NzInputModule,
    NzPopoverModule,
    NzModalModule
  ]
})
export class PlaylistDetailModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Playlist } from 'src/app/domain/model/playlist';
import { Song } from 'src/app/domain/model/song';
import { GetPlaylistUseCase } from 'src/app/domain/use-case/playlist/get-playlist.use-case';
import { DeleteSongUseCase } from 'src/app/domain/use-case/song/delete-song.use-case';
import { CreateSongUseCase } from 'src/app/domain/use-case/song/create-song.use-case';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DeletePlaylistUseCase } from 'src/app/domain/use-case/playlist/delete-playlist.use-case';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-playlist-detail',
  templateUrl: './playlist-detail.component.html',
  styleUrls: ['./playlist-detail.component.less']
})
export class PlaylistDetailComponent implements OnInit {

  name: string = '';
  playlist?: Playlist;
  error = '';

  constructor(
    private route: ActivatedRoute,
    private getPlaylistUseCase: GetPlaylistUseCase,
    private deleteSongUseCase: DeleteSongUseCase,
    private createSongUseCase: CreateSongUseCase,
    private message: MessageService,
    private nzModalService: NzModalService,
    private deletePlaylistUseCase: DeletePlaylistUseCase,
    private router: Router
  ) {
    this.route.params.subscribe(params => this.name = params['name']);
  }

  ngOnInit(): void {
    this.loadPlaylist();
  }

  async loadPlaylist() {
    try {
      this.playlist = await this.getPlaylistUseCase.execute(this.name);
    } catch (e: any) {
      this.error = e?.error?.message;
    }
  }

  async deleteSong(song: Song) {
    try {
      const result = await this.deleteSongUseCase.execute(song.id);
      if (this.playlist)
        this.playlist.songs = this.playlist.songs.filter(current => current.id !== song.id);
      this.message.create('success', result.message);
    } catch (e) {
      this.message.create('error', 'Ha ocurrido un error, intenta nuevamente');
    }

  }

  async createSong(song: Song) {
    if(this.playlist) {
      try {
        const result = await this.createSongUseCase.execute(this.playlist.id, song);
        this.playlist.songs = [...this.playlist.songs, result];
      } catch (e: any) {
        this.message.create('error', e.error?.message || 'Ha ocurrido un error. Intenta nuevamente');
      }
    }
  }

  async deletePlaylist() {
    try {
      if (this.playlist) {
        const result = await this.deletePlaylistUseCase.execute(this.playlist.nombreLista);
        this.message.create('success', 'Se eliminó la lista ' + this.playlist.nombreLista);
        this.router.navigate(['/', 'player', 'playlist']);
      }
    } catch (e: any) {
      this.message.create('error', e.error?.message || 'Ha ocurrido un error. Intenta nuevamente');
    }
  }

  confirmDelete() {
    this.nzModalService.create({
      nzContent: '¿Está seguro de eliminar esta lista?',
      nzOnOk: () => this.deletePlaylist()
    })
  }
}

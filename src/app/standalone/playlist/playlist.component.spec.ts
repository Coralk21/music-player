import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrimaryDirective } from 'src/app/directives/primary.directive';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { PlaylistItemComponent } from '../playlist-item/playlist-item.component';
import { UnorderedListOutline, PlayCircleOutline } from '@ant-design/icons-angular/icons';

import { PlaylistComponent } from './playlist.component';
import { NzIconModule } from 'ng-zorro-antd/icon';

describe('PlaylistComponent', () => {
  let component: PlaylistComponent;
  let fixture: ComponentFixture<PlaylistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        SharedModule,
        PlaylistItemComponent,
        PrimaryDirective,
        PlaylistComponent,
        NzIconModule.forRoot([ UnorderedListOutline, PlayCircleOutline ])
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should render items', () => {
    component.playlists = [
      {
        id: 0,
        nombreLista: 'Lista 1',
        descripcion: 'Descripcion',
        location: '',
        songs: []
      }
    ];
    fixture.detectChanges();
    const playlistElement: HTMLElement = fixture.nativeElement;
    const items = playlistElement.querySelectorAll('app-playlist-item');
    expect(items.length).toBe(1);
    expect(items[0].textContent).toContain('Lista 1');
    expect(items[0].textContent).toContain('Canciones: 0');
  });
});

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { PlaylistItemComponent } from '../playlist-item/playlist-item.component';
import { Playlist } from 'src/app/domain/model/playlist';
import { PrimaryDirective } from 'src/app/directives/primary.directive';

@Component({
  selector: 'app-playlist',
  standalone: true,
  imports: [
    CommonModule,
    SharedModule,
    PlaylistItemComponent,
    PrimaryDirective
  ],
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.less']
})
export class PlaylistComponent {
  @Input() playlists: Playlist[] = [];
  @Output() detail: EventEmitter<Playlist> = new EventEmitter<Playlist>();

  goToDetail(playlist: Playlist) {
    this.detail.emit(playlist);
  }
}

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { TextShortenerPipe } from 'src/app/pipes/text-shortener.pipe';

@Component({
  selector: 'app-playlist-item',
  standalone: true,
  imports: [
    CommonModule,
    NzIconModule,
    NzButtonModule,
    TextShortenerPipe
  ],
  templateUrl: './playlist-item.component.html',
  styleUrls: ['./playlist-item.component.less']
})
export class PlaylistItemComponent {
  @Input() name!: string;
  @Input() songs!: number;

  @Output() play: EventEmitter<void> = new EventEmitter();

  sendPlay(event: MouseEvent) {
    event.stopPropagation();
    this.play.emit();
  }
}

import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { TextShortenerPipe } from 'src/app/pipes/text-shortener.pipe';
import { UnorderedListOutline, PlayCircleOutline } from '@ant-design/icons-angular/icons';

import { PlaylistItemComponent } from './playlist-item.component';
import { By } from '@angular/platform-browser';

describe('PlaylistItemComponent', () => {
  let component: PlaylistItemComponent;
  let fixture: ComponentFixture<PlaylistItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        NzIconModule.forRoot([ UnorderedListOutline, PlayCircleOutline ]),
        NzButtonModule,
        TextShortenerPipe,
        PlaylistItemComponent
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaylistItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should display playlist name', () => {
    component.name = 'Playlist name';
    fixture.detectChanges();
    const playlistItemElement: HTMLElement = fixture.nativeElement;
    const nameElement = playlistItemElement.querySelector('.name');
    expect(nameElement?.textContent).toEqual(component.name);
  });

  it('Should display song length', () => {
    component.songs = 12;
    fixture.detectChanges();
    const playlistItemElement: HTMLElement = fixture.nativeElement;
    const nameElement = playlistItemElement.querySelector('.songs');
    expect(nameElement?.textContent).toEqual(`Canciones: ${component.songs}`);
  })
});

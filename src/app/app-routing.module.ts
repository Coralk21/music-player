import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule),
    canActivate: [AuthGuard],
    data: {
      public: true,
      redirectTo: ['/', 'player', 'playlist']
    }
  },
  {
    path: 'player',
    loadChildren: () => import('./pages/player/player.module').then(m => m.PlayerModule),
    canActivate: [AuthGuard],
    data: {
      public: false,
      redirectTo: ['/', 'auth', 'sign-in']
    }
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/auth/sign-in'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

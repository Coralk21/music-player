import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { ApiUrl } from '../data/constants/api-url';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app.reducer';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

  publicApis: string[] = [ApiUrl.SIGN_IN, ApiUrl.SIGN_UP];
  token? = '';

  constructor(private store: Store<AppState>) {
    this.store.select('token').subscribe({
      next: ({ value }) => this.token = value
    })
  }

  buildHost(request: HttpRequest<unknown>) {
    return request.clone({
      url: environment.apiHost + request.url
    });
  }

  buildAuth(request: HttpRequest<unknown>) {
    if (!this.publicApis.includes(request.url)) {
      return request.clone({
        headers: request.headers.append('Authorization', `Bearer ${this.token}`)
      })
    }
    return request;
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(
      this.buildHost(
        this.buildAuth(request)
      )
    );
  }
}

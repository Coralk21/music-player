import { createAction, props } from "@ngrx/store";
import { ActionGroup } from "src/app/data/constants/action-group";
import { Token } from "src/app/domain/model/token";

export const saveToken = createAction(`${ActionGroup.TOKEN} Save token`, props<Token>());
export const deleteToken = createAction(`${ActionGroup.TOKEN} Delete token`);
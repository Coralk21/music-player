import { Action, createReducer, on } from "@ngrx/store";
import { Token } from "src/app/domain/model/token";
import { deleteToken, saveToken } from "./token.actions";

export interface State {
    value?: string;
    expirationDate?: string;
}

export const initialState: State = {};

const _tokenReducer = createReducer(initialState,
    on(saveToken, (state: State, { token, expirationDate }: Token) => ({ ...state, value: token, expirationDate })),
    on(deleteToken, (state: State) => ({ ...state, token: undefined, expirationDate: undefined }))
);

export function tokenReducer(state: State = initialState, action: Action) {
    return _tokenReducer(state, action);
}
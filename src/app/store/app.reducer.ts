import { ActionReducerMap } from "@ngrx/store";
import * as token from './token/token.reducer';

export interface AppState {
    token: token.State
}

export const initialState: AppState = {
    token: {}
};

export const appReducers: ActionReducerMap<AppState> = {
    token: token.tokenReducer
};
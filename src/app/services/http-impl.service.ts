import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { HttpDataSource, Options } from '../data/data-source/http.data-source';

export class HttpImplService extends HttpDataSource {

  constructor(private http: HttpClient) {
    super();
  }

  get<T>(url: string, options?: Options | undefined): Promise<T> {
    return firstValueFrom(this.http.get<T>(url, options));
  }
  post<T>(url: string, body?: any, options?: Options | undefined): Promise<T> {
    return firstValueFrom(this.http.post<T>(url, body, options));
  }
  delete<T>(url: string, options?: Options | undefined): Promise<T> {
    return firstValueFrom(this.http.delete<T>(url, options));
  }
}

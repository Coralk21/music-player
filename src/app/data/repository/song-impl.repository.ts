import { Song } from "src/app/domain/model/song";
import { SongRepository } from "src/app/domain/repository/song.repository";
import { ApiUrl } from "../constants/api-url";
import { HttpDataSource } from "../data-source/http.data-source";

export class SongRepositoryImpl extends SongRepository {
    constructor(private http: HttpDataSource) {
        super();
    }

    deleteSong(id: number): Promise<{ message: string; }> {
        return this.http.delete<{ message: string}>(ApiUrl.SONG + id);
    }

    createSong(idPlaylist: number, song: Song): Promise<Song> {
        return this.http.post<Song>(ApiUrl.SONG, {
            idListaDeReproduccion: idPlaylist,
            ...song
        });
    }
}
import { Credentials } from "src/app/domain/model/credentials";
import { Token } from "src/app/domain/model/token";
import { AuthRepository } from "src/app/domain/repository/auth.repository";
import { ApiUrl } from "../constants/api-url";
import { HttpDataSource } from "../data-source/http.data-source";

export class AuthRepositoryImpl extends AuthRepository {

    constructor(private http: HttpDataSource) {
        super();
    }

    signIn(credentials: Credentials): Promise<Token> {
        return this.http.post<Token>(ApiUrl.SIGN_IN, credentials);
    }
}
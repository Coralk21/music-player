import { NewUserInput, NewUserOutput } from "src/app/domain/model/user";
import { UserRepository } from "src/app/domain/repository/user.repository";
import { ApiUrl } from "../constants/api-url";
import { HttpDataSource } from "../data-source/http.data-source";

export class UserRepositoryImpl extends UserRepository {

    constructor(private http: HttpDataSource) {
        super();
    }

    createUser(user: NewUserInput): Promise<NewUserOutput> {
        return this.http.post<NewUserOutput>(ApiUrl.SIGN_UP, user);
    }
}
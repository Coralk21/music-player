import { PlaylistInput, Playlist } from "src/app/domain/model/playlist";
import { PlaylistRepository } from "src/app/domain/repository/playlist.repository";
import { ApiUrl } from "../constants/api-url";
import { HttpDataSource } from "../data-source/http.data-source";

export class PlaylistRepositoryImpl extends PlaylistRepository {
    constructor(private http: HttpDataSource) {
        super();
    }

    createPlaylist(playlist: PlaylistInput): Promise<Playlist> {
        return this.http.post(ApiUrl.LISTS, playlist);
    }

    async getPlaylists(): Promise<Playlist[]> {
        const result = await this.http.get<{ listas: Playlist[]}>(ApiUrl.LISTS);
        return result.listas;
    }

    getPlaylist(name: string): Promise<Playlist> {
        return this.http.get<Playlist>(`${ApiUrl.LISTS}/${encodeURIComponent(name)}`);
    }

    deletePlaylist(name: string): Promise<void> {
        return this.http.delete(`${ApiUrl.LISTS}/${encodeURIComponent(name)}`);
    }
}
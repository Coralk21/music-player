export interface Options {
    headers: any;
    params: any;
}

export abstract class HttpDataSource {
    abstract get<T>(url: string, options?: Options): Promise<T>;
    abstract post<T>(url: string, body?: any, options?: Options): Promise<T>;
    abstract delete<T>(url: string, options?: Options): Promise<T>
}
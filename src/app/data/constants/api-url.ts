export class ApiUrl {
    static readonly AUTH = '/auth';
    static readonly SIGN_IN = `${ApiUrl.AUTH}/login`;
    static readonly SIGN_UP = `${ApiUrl.AUTH}/register`;
    static readonly LISTS = `/lists`;
    static readonly SONG = '/';
}
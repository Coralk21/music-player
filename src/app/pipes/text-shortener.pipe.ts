import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textShortener',
  standalone: true
})
export class TextShortenerPipe implements PipeTransform {

  transform(value: string, length: number = 15): string {
    if (value?.length <= length)
      return value;
    return value?.slice(0, length) + '...';
  }

}
